#!/usr/bin/env ruby
# coding: utf-8
# frozen_string_literal: true

require 'fileutils'

RESULTS_LOG = 'target/results.log'
ATTEMPTS = ENV.fetch('ATTEMPTS', 10).to_i

if ARGV.length.positive?
  FileUtils.rm_f(RESULTS_LOG)

  ATTEMPTS.times do |n|
    puts "--- Iteration #{n + 1}"
    ENV['QUIET'] = '1'
    exit unless system(*ARGV)
  end
end

Row = Struct.new(:name, :secs, :rss, :size)
FinalRow = Struct.new(:name, :secs, :rss, :size)

lines = File.open(RESULTS_LOG) do |f|
  f.readlines.map do |line|
    values = line.split("\t")
    Row.new(
      "#{values[0]}/#{values[1]}",
      values[2],
      values[3],
      values[4].strip
    )
  end
end
keys = lines.map(&:name).uniq

def median(array)
  return nil if array.empty?
  sorted = array.sort
  len = sorted.length
  (sorted[(len - 1) / 2] + sorted[len / 2]) / 2.0
end

def sd(list, scale=1, precision=2)
  list_median = median(list)
  deviations = list.map { |x| (x - list_median).abs }
  mad = median(deviations)
  format("%.#{precision}<median>f~±%05.#{precision}<mad>f~",
         median: list_median / scale, mad: mad / scale)
end

results = keys.map do |k|
  rows = lines.select { |line| line.name == k }
  abort("Integrity check failed (#{k})") if rows.length != ATTEMPTS
  secs = sd(rows.map { |row| row.secs.to_f }, 1e9, 3)
  rss = sd(rows.map { |row| row.rss.to_f }, 1_048_576)
  size = rows[0].size.to_i / 1024
  FinalRow.new(k, secs, rss, size)
end

results.sort! { |a, b| [a.secs.to_f, a.rss.to_f] <=> [b.secs.to_f, b.rss.to_f] }

table = [
  "|===",
  "|Binary |Time, s |RSS, MiB |Filesize, KiB",
  ""
]

results.each do |row|
  table << "|#{row.name}";
  table << "|#{row.secs}";
  table << "|#{row.rss}";
  table << "|#{row.size}";
  table << "";
end

table << "|===";
out = table.join("\n")
puts out

File.open("results.adoc", 'w') do |f|
  f.puts out
end
