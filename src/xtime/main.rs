mod select;

use byte_unit::Byte;
use libc::pid_t;
use procfs::process::{MMapPath, Process};
use std::convert::TryInto;
use std::fs::{self, OpenOptions};
use std::io::{ErrorKind, Read, Result, Write};
use std::net::{TcpListener, TcpStream};
use std::os::unix::io::AsRawFd;
use std::path::PathBuf;
use std::process::Command;
use std::time::{Duration, Instant};
use std::{cmp, env};

use select::FdSet;

struct Profiler {
    running: bool,
    pid: pid_t,
    rss: u128,
    started: Instant,
    name: String,
    platform: String,
    size: u128,
}

impl Profiler {
    fn update_rss(&mut self) {
        if let Ok(process) = Process::new(self.pid) {
            self.rss = cmp::max(self.rss, process.stat.rss_bytes().try_into().unwrap());
            let maps = process.maps().unwrap();
            let mut paths = maps.iter().filter_map(|map| {
                match &map.pathname {
                    MMapPath::Path(path) => Some(path),
                    _ => None
                }
            }).collect::<Vec<&PathBuf>>();
            paths.dedup();
            self.size = cmp::max(self.size, paths.iter().map(|path| {
                match fs::metadata(path) {
                    Ok(metadata) => metadata.len() as u128,
                    _ => 0
                }
            }).sum());
        }
    }

    fn start(&mut self, stream: &mut TcpStream) -> Result<()> {
        self.running = true;
        self.started = Instant::now();
        let mut buffer = String::new();
        stream.read_to_string(&mut buffer)?;
        let test_data = buffer.split('\t').collect::<Vec<&str>>();
        self.name = test_data[0].to_string();
        self.pid = test_data[1].parse::<libc::pid_t>().unwrap();
        self.platform = test_data[2].to_string();
        println!("{}/{}", self.name, self.platform);
        Ok(())
    }

    fn stop(&mut self) -> Result<()> {
        self.running = false;
        Ok(())
    }

    fn handle_client(&mut self, stream: &mut TcpStream) -> Result<()> {
        if self.running {
            self.stop()
        } else {
            self.start(stream)
        }
    }
}

pub fn to_string(bytes: u128) -> String {
    Byte::from_bytes(bytes)
        .get_appropriate_unit(true)
        .to_string()
}

fn main() -> Result<()> {
    let mut profiler = Profiler {
        running: false,
        pid: 0,
        rss: 0,
        started: Instant::now(),
        name: "".to_string(),
        platform: "".to_string(),
        size: 0
    };
    let listener = TcpListener::bind("127.0.0.1:9001")?;
    listener.set_nonblocking(true)?;

    let mut args = env::args().skip(1);
    let prog = args.next().unwrap();
    let prog_args = args.collect::<Vec<String>>();
    let mut cmd = Command::new(prog).args(prog_args).spawn()?;

    for stream in listener.incoming() {
        match stream {
            Ok(mut s) => {
                profiler.handle_client(&mut s)?;
                if !profiler.running {
                    break;
                }
            }
            Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
                let mut fd_set = FdSet::new();
                let raw_fd = listener.as_raw_fd();
                fd_set.set(raw_fd);
                select::select(
                    raw_fd + 1,
                    Some(&mut fd_set),
                    None,
                    None,
                    Some(&select::make_timeval(Duration::from_millis(10))),
                )?;
                profiler.update_rss();
                continue;
            }
            Err(e) => panic!("encountered IO error: {}", e),
        }
    }

    let elapsed = profiler.started.elapsed();
    let size = profiler.size;
    let stats = format!(
        "{:.3} s, {}, {}",
        elapsed.as_secs_f32(),
        to_string(profiler.rss),
        to_string(size)
    );
    let mut file = OpenOptions::new()
        .append(true)
        .create(true)
        .open("/opt/target/results.log")?;
    writeln!(
        file,
        "{}\t{}\t{}\t{}\t{}",
        profiler.name,
        profiler.platform,
        elapsed.as_nanos(),
        profiler.rss,
        size
    )?;
    cmd.wait()?;
    eprintln!("{}", stats);
    Ok(())
}
