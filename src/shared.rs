use std::io::Write;
use std::net::TcpStream;

pub fn notify(msg: &str) {
    if let Ok(mut stream) = TcpStream::connect("localhost:9001") {
        stream.write_all(msg.as_bytes()).ok();
    }
}
