use std::{env, process};

use libs_benchmark::primes;
use libs_benchmark::shared;

#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[no_mangle]
pub extern "C" fn __getauxval() -> u64 {
    0
}

fn main() {
    if primes::verify() {
        let mut args = env::args();
        let platform = args.nth(1).unwrap();

        shared::notify(&format!("Primes/jemalloc\t{}\t{}", process::id(), platform));
        let results = primes::find_all();
        shared::notify("stop");

        println!("{:?}", results);
    }
}
