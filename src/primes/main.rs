use std::{env, process};

use libs_benchmark::primes;
use libs_benchmark::shared;

fn main() {
    if primes::verify() {
        let mut args = env::args();
        let platform = args.nth(1).unwrap();

        shared::notify(&format!("Primes\t{}\t{}", process::id(), platform));
        let results = primes::find_all();
        shared::notify("stop");

        println!("{:?}", results);
    }
}
