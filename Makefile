.DEFAULT_GOAL := build

musl := target/musl
x86_64_musl := target/x86_64_musl
aarch64_musl := target/aarch64_musl

unwind := target/libunwind
x86_64_unwind := target/x86_64_libunwind
aarch64_unwind := target/aarch64_libunwind

x86_64_libs := target/x86_64/lib
x86_64_musl_lib := $(x86_64_libs)/libc.a
x86_64_unwind_lib := $(x86_64_libs)/libunwind.a

aarch64_libs := target/aarch64/lib
aarch64_musl_lib := $(aarch64_libs)/libc.a
aarch64_unwind_lib := $(aarch64_libs)/libunwind.a

# https://musl.libc.org/
MUSL_VERSION = 1.2.2
$(musl): | target
	cd target && \
	curl https://musl.libc.org/releases/musl-$(MUSL_VERSION).tar.gz \
	| tar -xz && \
	mv musl-$(MUSL_VERSION) musl

$(x86_64_musl) $(aarch64_musl): $(musl)
	cp -r $? $@

# https://github.com/libunwind/libunwind/releases
UNWIND_VERSION = 1.5.0
$(unwind): | target
	cd target && \
	curl -L https://github.com/libunwind/libunwind/releases/download/v1.5/libunwind-$(UNWIND_VERSION).tar.gz \
	| tar -xz && \
	mv libunwind-$(UNWIND_VERSION) libunwind

$(x86_64_unwind) $(aarch64_unwind): $(unwind)
	cp -r $? $@

$(x86_64_musl_lib): $(x86_64_musl)
	cd $? && \
	./configure --enable-optimize=size --prefix=`pwd`/../x86_64 && \
	make -j && make install

$(aarch64_musl_lib): $(aarch64_musl)
	cd $? && \
	CC=aarch64-linux-gnu-gcc \
	./configure --build x86_64-pc-linux-gnu --host aarch64-linux-gnu \
		--enable-optimize=size --prefix=`pwd`/../aarch64 && \
	make -j && make install

$(x86_64_unwind_lib): $(x86_64_unwind)
	cd $? && \
	CFLAGS=-Os ./configure --prefix=`pwd`/../x86_64 && \
	make -j && make install

$(aarch64_unwind_lib): $(aarch64_unwind)
	cd $? && \
	CC=aarch64-linux-gnu-gcc \
	CFLAGS=-Os ./configure --build x86_64-pc-linux-gnu \
		--host aarch64-linux-gnu --prefix=`pwd`/../aarch64 && \
	make -j && make install

.PHONY: libs
libs: $(x86_64_musl_lib) $(aarch64_musl_lib) $(x86_64_unwind_lib) $(aarch64_unwind_lib)

exe := primes xtime
srcs := src/*.rs src/xtime/*.rs src/primes/*.rs src/primes-jemalloc/*.rs Cargo.toml
x86_64_glib_target := x86_64-unknown-linux-gnu
x86_64_musl_target := x86_64-unknown-linux-musl
aarch64_musl_target := aarch64-unknown-linux-musl

x86_64_glib_release := target/$(x86_64_glib_target)/release
x86_64_glib_exe := $(addprefix $(x86_64_glib_release)/,$(exe))

x86_64_musl_release := target/$(x86_64_musl_target)/release
x86_64_musl_exe := $(addprefix $(x86_64_musl_release)/,$(exe))

aarch64_musl_release := target/$(aarch64_musl_target)/release
aarch64_musl_exe := $(addprefix $(aarch64_musl_release)/,$(exe))

CARGO_FLAGS = +nightly build --features xt,jm --release

$(x86_64_glib_exe): $(srcs)
	cargo $(CARGO_FLAGS) --target=$(x86_64_glib_target)

$(x86_64_musl_exe): $(srcs) $(x86_64_musl_lib) $(x86_64_unwind_lib)
	make libs
	RUSTFLAGS="-L`pwd`/target/x86_64/lib -L`gcc -print-search-dirs | sed -n 's/install: \(.*\)/\1/p'`" \
	cargo $(CARGO_FLAGS) --target=$(x86_64_musl_target)

$(aarch64_musl_exe): $(srcs) $(aarch64_musl_lib) $(aarch64_unwind_lib)
	make libs
	CC=aarch64-linux-gnu-gcc \
	RUSTFLAGS="-L`pwd`/target/aarch64/lib -L`aarch64-linux-gnu-gcc -print-search-dirs | sed -n 's/install: \(.*\)/\1/p'` -C link-arg=-lgcc" \
	cargo $(CARGO_FLAGS) --target=$(aarch64_musl_target)

executables := \
	target/x86_64-unknown-linux-gnu/release/primes \
	target/x86_64-unknown-linux-musl/release/primes \
	target/aarch64-unknown-linux-musl/release/primes \
	target/x86_64-unknown-linux-gnu/release/primes-jemalloc \
	target/x86_64-unknown-linux-musl/release/primes-jemalloc \
	target/aarch64-unknown-linux-musl/release/primes-jemalloc

all_runners := $(patsubst %,run[%], $(executables))

.PHONY: build
build: $(executables)

.PHONY: run
run: $(all_runners)

ECHO_RUN = @tput bold; echo "$(MAKE) $@"; tput sgr0

.PHONY: $(all_runners)
$(all_runners)::
	$(ECHO_RUN)

DOCKER_MULTIARCH = env docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
DOCKER_RUN = docker run -it --rm -v `pwd`:/opt

X86_64_GLIBC_RUN = $(DOCKER_RUN) frolvlad/alpine-glibc \
	/opt/target/x86_64-unknown-linux-gnu/release/xtime /opt/$^ x86_64-glibc

X86_64_MUSL_RUN = $(DOCKER_RUN) alpine \
	/opt/target/x86_64-unknown-linux-musl/release/xtime /opt/$^ x86_64-musl

AARCH64_MUSL_RUN = $(DOCKER_RUN) --platform linux/arm64 arm64v8/alpine \
	/opt/target/aarch64-unknown-linux-musl/release/xtime /opt/$^ aarch64-musl

run[target/x86_64-unknown-linux-gnu/release/primes]:: run[%]: %
	$(X86_64_GLIBC_RUN)

run[target/x86_64-unknown-linux-musl/release/primes]:: run[%]: %
	$(X86_64_MUSL_RUN)

run[target/aarch64-unknown-linux-musl/release/primes]:: run[%]: %
	$(DOCKER_MULTIARCH)
	$(AARCH64_MUSL_RUN)

run[target/x86_64-unknown-linux-gnu/release/primes-jemalloc]:: run[%]: %
	$(X86_64_GLIBC_RUN)

run[target/x86_64-unknown-linux-musl/release/primes-jemalloc]:: run[%]: %
	$(X86_64_MUSL_RUN)

run[target/aarch64-unknown-linux-musl/release/primes-jemalloc]:: run[%]: %
	$(DOCKER_MULTIARCH)
	$(AARCH64_MUSL_RUN)

.PHONY: fmt
fmt:
	cargo +nightly fmt

.PHONY: clippy
clippy:
	cargo +nightly clippy --features=xt,jm --target=x86_64-unknown-linux-gnu

.PHONY: clean
clean:
	cargo +nightly clean
